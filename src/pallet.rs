use std::collections::HashMap;
use Displayable;
use Texman;
use sdl2::event::Event;


pub struct Pallet<'a> {
	map: HashMap<String, Box<Displayable + 'a>>,
}

impl<'a> Pallet<'a> {
	pub fn new() -> Pallet<'a> {
		Pallet { map: HashMap::new() }
	}

	pub fn map(&mut self) -> &mut HashMap<String, Box<Displayable + 'a>> {
		&mut self.map
	}

	pub fn insert<F: Displayable + 'a>(&mut self, id: &str, obj: F) {
		self.map.insert(String::from(id), Box::new(obj));
	}
}

impl<'a> Displayable for Pallet<'a> {
	fn paint(&self, texman: &mut Texman) {
		for (_, e) in self.map.iter() {
			e.paint(texman);
		}
	}

	fn update(&mut self) {
		for (_, e) in self.map.iter_mut() {
			e.update();
		}
	}

	fn handle_events(&mut self, event: &Event, mouse_x: i32, mouse_y: i32) {
		for (_, e) in self.map.iter_mut() {
			e.handle_events(event, mouse_x, mouse_y);
		}
	}
}
