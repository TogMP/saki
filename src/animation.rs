use sdl2::rect::Rect;
use Displayable;
use Texman;


pub struct Animation {
	id: String,
	rect: Rect,
	frame_count: i32,
	frame_length: i32,
	frame_index: i32,
}

impl Animation {
	pub fn new(id: &str, rect: Rect, frame_count: i32, frame_length: i32) -> Animation {
		Animation {
			id: String::from(id),
			rect,
			frame_count,
			frame_length,
			frame_index: 0,
		}
	}

	pub fn id(&mut self) -> &mut String {
		&mut self.id
	}

	pub fn rect(&mut self) -> &mut Rect {
		&mut self.rect
	}
}

impl Displayable for Animation {
	fn paint(&self, texman: &mut Texman) {
		texman.draw(&self.id, self.rect);
		texman.draw_frame(
			&self.id,
			Rect::new(
				(self.frame_index / self.frame_length) * self.rect.width() as i32,
				0,
				self.rect.width(),
				self.rect.height(),
			),
			self.rect,
		);
	}

	fn update(&mut self) {
		if self.frame_index >= (self.frame_count * self.frame_length - 1) {
			self.frame_index = 0;
		} else {
			self.frame_index += 1;
		}
	}
}
