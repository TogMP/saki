use sdl2::render::{Canvas, Texture, TextureCreator};
use sdl2::video::{Window, WindowContext};
use sdl2::image::LoadTexture;
use sdl2::rect::{Point, Rect};
use sdl2::{init, Sdl, VideoSubsystem};
use std::collections::hash_map::{Entry, HashMap};
use std::path::Path;


pub struct Texman<'a> {
	texture_map: HashMap<String, Texture<'a>>,
	font_map: HashMap<String, Font>,
	canvas: Canvas<Window>,
	creator: &'a TextureCreator<WindowContext>,
}

impl<'a> Texman<'a> {
	pub fn new(canvas: Canvas<Window>, creator: &'a TextureCreator<WindowContext>) -> Texman<'a> {
		Texman {
			texture_map: HashMap::new(),
			font_map: HashMap::new(),
			canvas,
			creator,
		}
	}

	pub fn get_canvas(&mut self) -> &mut Canvas<Window> {
		&mut self.canvas
	}

	pub fn load_image(&mut self, path: &str, id: &str) {
		match self.texture_map.entry(id.to_string()) {
			Entry::Vacant(e) => {
				match self.creator.load_texture(Path::new(path)) {
					Ok(tex) => {
						e.insert(tex);
					}
					Err(msg) => {
						println!("Texture not found {}", msg);
					}
				}
			}
			_ => {}
		}
	}

	pub fn gen_font_map(&mut self, id: &str, width: u32, height: u32) {
		let mut build: HashMap<char, Rect> = HashMap::new();
		// Generates the masks for all numbers
		for x in 0..10 {
			build.insert(
				char::from(x as u8),
				Rect::new(x * width as i32, 0, width, height),
			);
		}

		// Generates the masks for all upper case letters
		let mut c: i32 = 0;
		for x in "ABCDEFGHIJKLMNOPQRSTUVWXYZ".chars() {
			build.insert(x, Rect::new(c * width as i32, height as i32, width, height));
			c += 1;
		}

		// Generates the masks for all lower case numbers
		c = 0;
		for x in "abcdefghijklmnopqrstuvwxyz".chars() {
			build.insert(
				x,
				Rect::new(c * width as i32, 2 * height as i32, width, height),
			);
			c += 1;
		}

		// Generates the masks for the first set of special characters
		c = 0;
		for x in "#^*$".chars() {
			build.insert(x, Rect::new((c + 10) * width as i32, 0, width, height));
			c += 1;
		}

		// Generates the masks for the rest of the special characters
		c = 0;
		for x in "?.,'\"!-()[]:;<>/\\{}|_%=+~ ".chars() {
			build.insert(
				x,
				Rect::new(c * width as i32, 3 * height as i32, width, height),
			);
			c += 1;
		}

		// This is the unicode error symbol
		build.insert(
			'\u{FFFD}',
			Rect::new(c + 25 * width as i32, 0, width, height),
		);

		let insert = Font {
			width,
			height,
			map: build,
		};

		self.font_map.insert(String::from(id), insert);
	}

	pub fn draw(&mut self, id: &str, dest: Rect) {
		match self.texture_map.entry(id.to_string()) {
			Entry::Occupied(tex) => {
				self.canvas.copy(tex.get(), None, Some(dest)).unwrap();
			}
			_ => {}
		}
	}

	pub fn draw_frame(&mut self, id: &str, src: Rect, dest: Rect) {
		match self.texture_map.entry(id.to_string()) {
			Entry::Occupied(tex) => {
				match self.canvas.copy(tex.get(), Some(src), Some(dest)) {
					Ok(_) => {}
					Err(msg) => {
						panic!("{}", msg);
					}
				}
			}
			_ => {}
		}
	}

	pub fn write(&mut self, text: &str, font_id: &str, font_map_id: &str, dest: Point) {
		let font = self.font_map.get(font_map_id).unwrap().map.clone();
		let mut c: i32 = 0;
		for a in text.chars() {
			let temp = match font.get(&a) {
				Some(e) => *e,
				None => *font.get(&'\u{FFFD}').unwrap(),
			};

			let width = self.font_map.get(font_map_id).unwrap().width.clone();
			let height = self.font_map.get(font_map_id).unwrap().height.clone();
			self.draw_frame(
				font_id,
				Rect::new(dest.x() + (c * width as i32), dest.y(), width, height),
				temp,
			);
			c += 1;
		}
	}
}

pub fn gen_context(
	label: &str,
	w: u32,
	h: u32,
) -> (Sdl, VideoSubsystem, Canvas<Window>, TextureCreator<WindowContext>) {
	let context = init().unwrap();
	let video = context.video().unwrap();
	let window = video
		.window(label, w, h)
		.position_centered()
		.opengl()
		.build()
		.unwrap();
	let canvas = window.into_canvas().build().unwrap();
	let creator = canvas.texture_creator();
	(context, video, canvas, creator)
}

pub struct Font {
	pub width: u32,
	pub height: u32,
	pub map: HashMap<char, Rect>,
}
