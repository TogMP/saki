use sdl2::rect::{Rect, Point};
use sdl2::event::Event;
use Displayable;
use Texman;


pub struct Button<'a> {
	id: String,
	id_clicked: String,
	is_clicked: bool,
	rect: Rect,
	closure: Option<Box<FnMut() + 'a>>,
}

#[derive(Clone)]
pub struct Sprite {
	id: String,
	rect: Rect,
}

impl<'a> Button<'a> {
	pub fn new(id: &str, id_clicked: &str, rect: Rect) -> Button<'a> {
		Button {
			id: String::from(id),
			id_clicked: String::from(id_clicked),
			is_clicked: false,
			rect,
			closure: None,
		}
	}

	pub fn connect<F>(&mut self, c: F)
	where
		F: 'a,
		F: FnMut(),
	{
		self.closure = Some(Box::new(c));
	}

	pub fn rect(&self) -> Rect {
		self.rect
	}

	pub fn rect_mut(&mut self) -> &mut Rect {
		&mut self.rect
	}

	pub fn callback(&mut self, x: i32, y: i32) {
		if self.rect.contains_point(Point::new(x, y)) && self.is_clicked {
			match self.closure {
				None => {
					println!("No callback for function {}", self.id);
				}
				Some(ref mut c) => {
					c();
				}
			}
		}
	}

	pub fn update_sprite(&mut self, x: i32, y: i32) {
		if self.rect.contains_point(Point::new(x, y)) {
			self.is_clicked = true;
		} else {
			self.is_clicked = false;
		}
	}

	pub fn release(&mut self) {
		self.is_clicked = false;
	}

	pub fn is_clicked(&self) -> bool {
		self.is_clicked
	}
}

impl<'a> Displayable for Button<'a> {
	fn paint(&self, texman: &mut Texman) {
		if self.is_clicked == false {
			texman.draw(&self.id, self.rect);
		} else {
			texman.draw(&self.id_clicked, self.rect);
		}
	}

	fn handle_events(&mut self, event: &Event, mouse_x: i32, mouse_y: i32) {
		match event {
			&Event::MouseButtonDown { .. } => {
				self.update_sprite(mouse_x, mouse_y);
			}
			&Event::MouseButtonUp { .. } => {
				self.callback(mouse_x, mouse_y);
				self.release();
			}
			_ => {}
		}
	}
}

impl Sprite {
	pub fn new(id: &str, rect: Rect) -> Sprite {
		Sprite {
			id: String::from(id),
			rect,
		}
	}

	pub fn id(&mut self) -> &mut String {
		&mut self.id
	}

	pub fn rect(&mut self) -> &mut Rect {
		&mut self.rect
	}
}

impl Displayable for Sprite {
	fn paint(&self, texman: &mut Texman) {
		texman.draw(&self.id, self.rect);
	}
}
