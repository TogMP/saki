use texman::Texman;
use sdl2::event::Event;


pub trait Displayable {
	fn paint(&self, texman: &mut Texman);

	fn update(&mut self) {}

	fn handle_events(&mut self, _event: &Event, _mouse_x: i32, _mouse_y: i32) {}
}
