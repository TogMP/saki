use sdl2::rect::Point;
use Displayable;
use Texman;


pub struct Label {
	point: Point,
	text: String,
	style: String,
}

impl Label {
	pub fn new(x: i32, y: i32, text: &str, style: &str) -> Label {
		Label {
			point: Point::new(x, y),
			text: String::from(text),
			style: String::from(style),
		}
	}
}

impl<'a> Displayable for Label {
	fn paint(&self, texman: &mut Texman) {
		texman.write(&self.text, &self.style, &self.style, self.point);
	}
}
