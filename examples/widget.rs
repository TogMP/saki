extern crate saki;
extern crate sdl2;

use saki::*;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::rect::Rect;
use sdl2::pixels::Color;
use std::cell::RefCell;


fn main() {
	let (context, _, canvas, creator) = gen_context("Widgets", 400, 400);
	let mut texman = Texman::new(canvas, &creator);
	let mut event_pump = context.event_pump().unwrap();
	texman.load_image("assets/button.png", "button");
	texman.load_image("assets/button_down.png", "button_down");
	texman.load_image("assets/win.png", "win");

	let r = RefCell::new(0);

	let mut button = Button::new("button", "button_down", Rect::new(0, 0, 60, 40));
	button.connect(|| {
		let t = *r.borrow();
		*r.borrow_mut() = t + 1;
		println!("{}", r.borrow());
	});
	let sprite = Sprite::new("win", Rect::new(0, 0, 400, 400));

	texman.get_canvas().set_draw_color(
		Color::RGB(255, 255, 255),
	);
	texman.get_canvas().clear();
	texman.get_canvas().present();

	let mut pal = Pallet::new();
	pal.insert("sprite", sprite);
	pal.insert("button", button);
	'main: loop {
		let state = event_pump.mouse_state();
		for event in event_pump.poll_iter() {
			match event {
				Event::Quit { .. } => {
					break 'main;
				}
				Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
					break 'main;
				}
				_ => {}
			}
			pal.handle_events(&event, state.x(), state.y());
		}

		texman.get_canvas().clear();
		pal.paint(&mut texman);
		texman.get_canvas().present();
	}
}
