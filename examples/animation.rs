extern crate saki;
extern crate sdl2;

use saki::*;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::rect::Rect;


fn main() {
	let (context, _, canvas, creator) = gen_context("Animation", 400, 400);
	let mut texman = Texman::new(canvas, &creator);
	let mut event_pump = context.event_pump().unwrap();
	texman.load_image("assets/strip.png", "strip");
	let mut anim = Animation::new("strip", Rect::new(0, 0, 200, 200), 8, 1000);

	'main: loop {
		for event in event_pump.poll_iter() {
			match event {
				Event::Quit { .. } => {
					break 'main;
				}
				Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
					break 'main;
				}
				_ => {}
			}
		}
		anim.update();
		texman.get_canvas().clear();
		anim.paint(&mut texman);
		texman.get_canvas().present();
	}
}
