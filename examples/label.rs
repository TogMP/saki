extern crate saki;
extern crate sdl2;

use saki::*;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;


fn main() {
	let (context, _, canvas, creator) = gen_context("Label", 400, 400);
	let mut texman = Texman::new(canvas, &creator);
	let mut event_pump = context.event_pump().unwrap();
	texman.load_image("assets/font.png", "font");
	texman.gen_font_map("font", 6, 14);

	let test_text = Label::new(0, 0, "Hello world!", "font");

	texman
		.get_canvas()
		.set_draw_color(Color::RGB(255, 255, 255));
	texman.get_canvas().clear();
	texman.get_canvas().present();

	'main: loop {
		for event in event_pump.poll_iter() {
			match event {
				Event::Quit { .. } => {
					break 'main;
				}
				Event::KeyDown {
					keycode: Some(Keycode::Escape),
					..
				} => {
					break 'main;
				}
				_ => {}
			}
		}

		texman.get_canvas().clear();
		test_text.paint(&mut texman);
		texman.get_canvas().present();
	}
}
