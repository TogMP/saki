extern crate saki;
extern crate sdl2;

use saki::*;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::rect::Rect;
use sdl2::pixels::Color;
use std::cell::RefCell;
use std::collections::HashMap;


fn main() {
	let (context, _, canvas, creator) = gen_context("Pages", 400, 400);
	let mut texman = Texman::new(canvas, &creator);
	let mut event_pump = context.event_pump().unwrap();
	texman.load_image("assets/button.png", "button");
	texman.load_image("assets/button_down.png", "button_down");
	texman.load_image("assets/win.png", "win");

	let page = RefCell::new(String::from("bt"));
	let mut button = Button::new("button", "button_down", Rect::new(0, 0, 60, 40));
	button.connect(|| { *page.borrow_mut() = String::from("win"); });
	let mut bt = Pallet::new();
	bt.insert("button", button);

	let mut sprite = Button::new("win", "win", Rect::new(0, 0, 400, 400));
	sprite.connect(|| { *page.borrow_mut() = String::from("bt"); });
	let mut win = Pallet::new();
	win.insert("sprite", sprite);

	let mut switcher: HashMap<String, Pallet> = HashMap::new();
	switcher.insert(String::from("bt"), bt);
	switcher.insert(String::from("win"), win);

	texman.get_canvas().set_draw_color(
		Color::RGB(255, 255, 255),
	);
	texman.get_canvas().clear();
	texman.get_canvas().present();

	'main: loop {
		let state = event_pump.mouse_state();
		for event in event_pump.poll_iter() {
			match event {
				Event::Quit { .. } => {
					break 'main;
				}
				Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
					break 'main;
				}
				_ => {}
			}
			let t = page.borrow().clone();
			switcher.get_mut(&*t).unwrap().handle_events(
				&event,
				state.x(),
				state.y(),
			);
		}

		texman.get_canvas().clear();
		switcher.get_mut(&*page.borrow()).unwrap().paint(
			&mut texman,
		);
		texman.get_canvas().present();
	}
}
