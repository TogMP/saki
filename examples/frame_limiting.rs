extern crate saki;
extern crate sdl2;

use saki::*;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::rect::Rect;
use std::time::*;
use std::thread;

pub enum Action {
    Stop,
    Continue,
}

pub fn start_loop<F>(mut callback: F) where F: FnMut() -> Action {
    let mut accumulator = Duration::new(0, 0);
    let mut previous_clock = Instant::now();

    loop {
        match callback() {
            Action::Stop => break,
            Action::Continue => ()
        };

        let now = Instant::now();
        accumulator += now - previous_clock;
        previous_clock = now;

        let fixed_time_stamp = Duration::new(0, 16666667);
        while accumulator >= fixed_time_stamp {
            accumulator -= fixed_time_stamp;
        }

        thread::sleep(fixed_time_stamp - accumulator);
    }
}

fn main() {
	let (context, _, canvas, creator) = gen_context("Animation", 400, 400);
	let mut texman = Texman::new(canvas, &creator);
	let mut event_pump = context.event_pump().unwrap();
	texman.load_image("assets/strip.png", "strip");
	let mut anim = Animation::new("strip", Rect::new(0, 0, 200, 200), 8, 60);

    start_loop(|| {
        let mut action = Action::Continue;
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => {
                    action = Action::Stop;
                }
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    action = Action::Stop;
                }
                _ => {}
            }
        }
        anim.update();
        texman.get_canvas().clear();
        anim.paint(&mut texman);
        texman.get_canvas().present();
        action
    });
}
